package com.itheima.demo.command;

import javax.swing.*;

//接收者：肠粉厨师
class ChangFenChef extends JFrame {
    JLabel l = new JLabel();

    ChangFenChef() {
        super("煮肠粉");
        l.setIcon(new ImageIcon("hystrix-client/src/main/resources/ChangFen.jpg"));
        this.add(l);
        this.setLocation(30, 30);
        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void cooking() {
        this.setVisible(true);
    }
}
