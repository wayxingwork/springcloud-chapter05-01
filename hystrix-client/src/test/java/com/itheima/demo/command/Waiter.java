package com.itheima.demo.command;

import com.itheima.demo.command.Breakfast;

//调用者：服务员
class Waiter {
    private Breakfast changFen,heFen;

    public void chooseChangFen() {
        changFen.cooking();
    }
    public void chooseHeFen() {
        heFen.cooking();
    }

    public void setChangFen(Breakfast f) {
        changFen = f;
    }
    public void setHeFen(Breakfast f) {
        heFen = f;
    }
}
