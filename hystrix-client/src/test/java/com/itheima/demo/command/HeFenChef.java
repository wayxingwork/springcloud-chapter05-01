package com.itheima.demo.command;

import javax.swing.*;

//接收者：肠粉厨师
class HeFenChef extends JFrame {
    JLabel l = new JLabel();

    HeFenChef() {
        super("煮河粉");
        l.setIcon(new ImageIcon("hystrix-client/src/main/resources/HeFen.jpg"));
        this.add(l);
        this.setLocation(30, 30);
        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void cooking() {
        this.setVisible(true);
    }
}
