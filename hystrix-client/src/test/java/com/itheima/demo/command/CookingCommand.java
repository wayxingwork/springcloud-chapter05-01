package com.itheima.demo.command;

import com.itheima.demo.command.Breakfast;

public class CookingCommand {
    public static void main(String[] args) {
        Breakfast food1 = new ChangFen();
        Waiter fwy = new Waiter();
        fwy.setChangFen(food1);//设置肠粉菜单
        fwy.chooseChangFen();  //选择肠粉

        Breakfast food2 = new HeFen();
        fwy.setHeFen(food2);//设置肠粉菜单
        fwy.chooseHeFen();  //选择肠粉
    }
}
