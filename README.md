# springcloud-chapter05-01

#### 介绍
第五章熔断 Hystrix

#### 软件架构
软件架构说明


#### 安装教程

1. 运行 Eureka Server
2. 运行 hello-provider
3. 运行 hystrix-client,发请求测试效果
4. 停止 hello-provider
5. 发请求测试效果

#### 效果

![](./doc/1-1.png)

停止 hello-provider 后的效果
![](./doc/1-2.png)